# Proyecto [Chaucha](http://chaucha.cl)

El objetivo principal del Proyecto Chaucha es educar a la población Chilena sobre la utilización de criptomonedas, 
proponiendo un sistema monetario confiable, transparente y descentralizado, 
junto con la documentación necesaria para comprender de manera simple el funcionamiento de la red y la tecnología utilizada. 

## Chauchera

![Chaucha](https://gitlab.com/chaucha/chauchera/uploads/1a860cfde1be9b609e3d54283abfe494/oJpr6JC.png)

Wallet oficial del [Proyecto Chaucha](http://chaucha.cl).

## ¿Qué es esto?

Chauchera es un cliente modificado desde el código fuente de [Litecoin](https://litecoin.org/) (0.8.7.4), que permite a las personas comunicarse con la red del proyecto Chaucha.

Con la Chauchera podrás enviar y recibir Chauchas (CHA), que es la criptomoneda utilizada dentro de la red.

## ¿Por qué el nombre "chauchera"?

En Chile es común llamarle "chauchera" a un pequeño bolso utilizado para almacenar monedas, y como la criptomoneda se llama Chaucha, lo normal era llamarle Chauchera a la wallet.

## Información de la Red Chaucha:

- Cada un minuto se generan Chauchas como recompensa al minar un bloque.
- Con el primer bloque se generaron **50.000 CHA** para el financiamiento del proyecto.
- Desde el bloque `#1` al `#2833` la recompensa fue de **10 CHA** por bloque.
- A partir del bloque `#2834` hasta el bloque `#5000` la recompensa es variable.
- Se recalcula la dificultad de minado de bloques cada **30 minutos**.
- El máximo de Chauchas que existirán en la red es de `123.456.789 CHA`.
- La Red Chaucha está basada en [Litecoin](https://litecoin.org/).
